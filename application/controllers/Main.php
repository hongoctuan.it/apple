<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Main extends MY_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->Model = $this->M_myweb->set_table('category');
	}
	public function index()
	{
		$this->data['title']	= "Trang Chủ";
		$this->data['category'] = $this->Model->set('deleted',0)->set_orderby('id')->gets();
		$this->data['subview'] 	= 'default/index/V_index';
		$this->load->view('default/_main_page', $this->data);
	}
}
