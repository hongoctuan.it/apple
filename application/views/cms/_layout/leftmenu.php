<!-- begin .app-side -->
<aside class="app-side">
	<!-- begin .side-content -->
	<div class="side-content">
		<!-- begin user-panel -->
		<div class="user-panel">
			<div class="user-image">
				<a href="#">
					<img class="img-circle" src="<?php echo $avatar;?>" alt="<?php echo $infoLog->userName?>">
				</a>
			</div>
			<div class="user-info">
				<h5><?php echo $infoLog->userName?></h5>
				<ul class="nav">
					<li class="dropdown">
						<a href="#" class="text-turquoise small dropdown-toggle bg-transparent" data-toggle="dropdown">
							<i class="fa fa-fw fa-circle">
							</i> Online
						</a>
						<ul class="dropdown-menu animated flipInY pull-right">
							<li>
								<a href="<?php echo site_url('admin/user?act=profile&id='.$infoLog->logid."&token=".$infoLog->token)?>">Profile</a>
							</li>
							<li role="separator" class="divider"></li>
							<li>
								<a href="<?php echo site_url('admin/logout')?>">
									<i class="fa fa-fw fa-sign-out"></i> Logout
								</a>
							</li>
						</ul>
					</li>
				</ul>
			</div>
		</div>
		<!-- END: user-panel -->
		<!-- begin .side-nav -->
		<nav class="side-nav">
			<!-- BEGIN: nav-content -->
			<ul class="metismenu nav nav-inverse nav-bordered nav-stacked" data-plugin="metismenu">
				<li class="nav-divider"></li>
				<!-- BEGIN: Media -->
				<?php if(checkcontroller("category")){?>
				<li>
					<a href="<?php echo site_url('admin/category')?>" <?php echo $this->controller=="category"?'class="active"':''?> >
						<span class="nav-icon">
							<i class="fa fa-fw fa-cubes"></i>
						</span>
						<span class="nav-title">Danh Mục</span>
					</a>
				</li>
				<?php } ?>
				<!-- END: Media -->

				<!-- BEGIN: user -->
				<?php if(checkcontroller("user")){?>
				<li>
					<a href="<?php echo site_url('admin/user')?>" <?php echo $this->controller=="user"?'class="active"':''?> >
						<span class="nav-icon">
							<i class="fa fa-fw fa-user"></i>
						</span>
						<span class="nav-title">User</span>
					</a>
				</li>
				<?php } ?>
				<!-- END: user -->
			</ul>
			<!-- END: nav-content -->
		</nav>
		<!-- END: .side-nav -->
	</div>
	<!-- END: .side-content -->
</aside>
<!-- END: .app-side -->