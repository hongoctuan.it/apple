<div class="container content">
    <?php foreach($category as $item): ?>
        <section id="<?php echo $item->slug?>">
            <img src="<?php echo site_url("assets/public/avatar/").$item->img?>">
            <?php echo $item->description; ?>
        </section>
    <?php endforeach;?>
</div>
