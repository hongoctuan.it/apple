<!DOCTYPE html>
<html lang="en">

<head>
    <!-- <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"> -->
    <link rel="stylesheet" href="<?php echo base_url();?>statics/default/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>statics/default/css/index.css">
    <title>Tao</title>
    <link rel="shortcut icon" type="ico" href="assets/applelogo.jpeg" />


</head>

<body>
<div class="menu">
    <div class="menucontent">
        <ul class="row">
            <a href="#" class="col-md-1 logo">
                <li class="menu-item">
                    <img src="<?php echo base_url();?>assets/applelogo.jpeg" height="60px" style="margin-right:30px; float:left"/>
                </li>
            </a>
            <?php foreach($category as $item):?>
                <?php if($item->id!=1):?>
                    <a href="#<?php echo $item->slug?>" class="col-md-2"><li class="menu-item"><?php echo $item->name?></li></a>
                <?php endif;?>
            <?php endforeach;?>
        </ul>
    </div>
</div>